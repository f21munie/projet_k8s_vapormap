# Descriptif du projet

- Auteur : Florian MUNIER
- Contact : florian.munier@imt-atlantique.net
- Formation : MS Infrastructures Cloud et DevOps
- Date : 31/01/2023 - 20/02/2023
- Description : Ce projet a pour but de déployer l'application Vapormap localement en utilisant Kubernetes

# Clone du projet

```sh
git clone https://gitlab.imt-atlantique.fr/f21munie/projet_k8s_vapormap.git
```

# Installation de l'environnement

Tout d'abord, si ce n'est pas déjà fait, installez snap :

```sh
sudo snap install microk8s --classic
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube
su - $USER
```
Vérifiez que le cluster Kubernetes fonctionne correctement :

```sh
microk8s status --wait-ready
```

Vous pouvez récupérer la liste des noeuds (un seul par défaut) :

```sh
microk8s kubectl get nodes
```

Afin de faciliter la suite et de rester vanilla, ajouter l'alias suivant :

```sh
alias kubectl='microk8s kubectl'
echo "alias kubectl='microk8s kubectl'" >> ~/.bashrc
```

Ajoutez la complétion de la commande kubectl :

```sh
echo 'source <(kubectl completion bash)' >>~/.bashrc
bash
```

# Déploiement de Vapormap

```sh
kubectl apply -f registry-credentials.yml

kubectl apply -f mariadb.yml
kubectl apply -f api.yml
kubectl apply -f front.yml

kubectl apply -f ingress.yml
```

# Visualiser les pods

```sh
kubectl get pods -w
```

# Accéder à Vapormap

Pour accéder à l'application Vapormap, rendez-vous sur les adresses suivantes :

- Frontend
```sh
http://localhost
```
- API
```sh
http://localhost/api/points/
```
